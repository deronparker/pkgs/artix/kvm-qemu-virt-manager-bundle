The point of this package is to be able to `pacman -Rs` it and remove all the dependencies at once.

Post-installation:
- your user needs to be in the `libvirt` group
- `libvirtd` and `virtlogd` should be running when you are trying to use `virt-manager`
- a reboot is probably needed after first install
